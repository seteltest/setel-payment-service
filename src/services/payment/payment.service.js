/**
 * Payment Service
 *
 */
const { Op } = require('sequelize');
const { PAYMENT_STATUS } = require('@config/vars');
const { APIError } = require('@utils/APIError');
const { throwReadableDBError } = require('@utils/error-handling');
const { Payment } = require('@models');

/**
 * Function to create payment
 * @param {Object} body         Order Detail
 *
 * @returns Payment Object
 *
 * @public
 */
const createPayment = async (body) => {
  try {
    const paymentOption = {
      orderId: body.id,
      currency: body.currency,
      amountToPay: body.amountToBePaid,
      status: PAYMENT_STATUS.PROCESSING
    };
    const payment = await Payment.create(paymentOption);
    return payment;
  } catch (err) {
    throw throwReadableDBError(err, 'PAYMENT_NOT_CREATED');
  }
};

/**
 * Function to get the detail of payment for a particular order
 * @param {UUID} orderId          Order ID
 *
 * @returns Payment Object
 *
 * @public
 */
const getPayment = async (orderId) => {
  try {
    const payment = await Payment.findOne({
      where: { orderId: { [Op.eq]: orderId } }
    });
    if (!payment) {
      throw APIError.withCode('PAYMENT_NOT_FOUND');
    }
    return payment;
  } catch (err) {
    throw throwReadableDBError(err, 'PAYMENT_SERVICE_DOWN');
  }
};

/**
 * Function to update the payment status
 * @param {UUID} orderId                  Order ID
 * @param {String} status                 Payment Status
 * @param {UUID} transactionId            Transaction ID
 *
 * @returns Payment Object
 *
 * @public
 */
const updatePaymentStatus = async (orderId, status, transactionId) => {
  try {
    const [rowsUpdate, [updatedPayment]] = await Payment.update(
      {
        status, transactionId
      },
      {
        returning: true,
        where: {
          orderId: { [Op.eq]: orderId },
          status: { [Op.eq]: PAYMENT_STATUS.PROCESSING }
        }
      }
    );
    if (!rowsUpdate) {
      throw APIError.withCode('PAYMENT_NOT_FOUND');
    }
    return updatedPayment;
  } catch (err) {
    throw throwReadableDBError(err, 'PAYMENT_NOT_UPDATED');
  }
};

module.exports = {
  createPayment,
  getPayment,
  updatePaymentStatus
};
