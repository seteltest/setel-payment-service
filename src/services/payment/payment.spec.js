/* eslint-disable arrow-body-style */
const service = require('./payment.service');

jest.mock('@models');
const { Payment } = require('@models');

describe('Service - payment', () => {
  beforeEach(() => {});

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should failed to create payment', (done) => {
    Payment.create = jest.fn(() => Promise.reject(new Error('Oops')));
    return service.createPayment({}).catch((err) => {
      expect(err.errors[0].errorCode).toBe('PAYMENT_NOT_CREATED');
      done();
    });
  });

  it('should create new payment', (done) => {
    Payment.create = jest.fn(() => Promise.resolve({}));
    return service.createPayment({}).then((response) => {
      expect(response).toBeObject();
      done();
    });
  });

  it('should failed to get payment', (done) => {
    Payment.findOne = jest.fn(() => Promise.reject(new Error('Oops')));
    return service.getPayment({}).catch((err) => {
      expect(err.errors[0].errorCode).toBe('PAYMENT_SERVICE_DOWN');
      done();
    });
  });

  it('should failed if payment not found', (done) => {
    Payment.findOne = jest.fn(() => Promise.resolve(null));
    return service.getPayment({}).catch((err) => {
      expect(err.errors[0].errorCode).toBe('PAYMENT_NOT_FOUND');
      done();
    });
  });

  it('should return payment detail', (done) => {
    Payment.findOne = jest.fn(() => Promise.resolve({}));
    return service.getPayment({}).then((response) => {
      expect(response).toBeObject();
      done();
    });
  });

  it('should failed to update payment', (done) => {
    Payment.update = jest.fn(() => Promise.reject(new Error('Oops')));
    return service.updatePaymentStatus(1, 'CONFIRMED', 1).catch((err) => {
      expect(err.errors[0].errorCode).toBe('PAYMENT_NOT_UPDATED');
      done();
    });
  });

  it('should failed to find payment', (done) => {
    Payment.update = jest.fn(() => Promise.resolve([0, [{}]]));
    return service.updatePaymentStatus(1, 'CONFIRMED', 1).catch((err) => {
      expect(err.errors[0].errorCode).toBe('PAYMENT_NOT_FOUND');
      done();
    });
  });

  it('should update the payment status', (done) => {
    Payment.update = jest.fn(() => Promise.resolve([1, [{}]]));
    return service.updatePaymentStatus(1, 'CONFIRMED', 1).then((response) => {
      expect(response).toBeObject();
      done();
    });
  });
});
