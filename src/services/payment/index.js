const {
  createPayment,
  getPayment,
  updatePaymentStatus
} = require('./payment.service');

module.exports = {
  createPayment,
  getPayment,
  updatePaymentStatus
};
