/* eslint-disable arrow-body-style */
const MockRes = require('mock-express-response');
const httpStatus = require('http-status');
const util = require('./helper.util');

describe('Utility - helper', () => {
  const res = new MockRes();

  beforeEach(() => {});

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should return OK response', () => {
    const status = jest.spyOn(res, 'status');
    const json = jest.spyOn(res, 'json');
    util.OK(res);
    expect(status).toBeCalledWith(httpStatus.OK);
    expect(json).toBeCalledWith(expect.objectContaining({
      responseCode: httpStatus.OK,
      responseMessage: expect.any(String)
    }));
  });

  it('should return the random number', () => {
    const random = util.randomNumberGenerator(1, 10);
    expect(random).toBeNumber();
  });
});
