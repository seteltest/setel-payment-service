/**
 * Helper Utility
 *
 */
const httpStatus = require('http-status');

/**
 * Utility function to handle response
 * @param {Object} res                        Response object of express
 * @param {String} responseMessage            The response message which needs to send
 * @param {any} response                      The response object which needs to send
 * @param {Number} statusCode                 The status code of the request
 */
const OK = (res, responseMessage = 'OK', response = {}, status = httpStatus.OK) => {
  res.status(status);
  return res.json({
    responseCode: status,
    responseMessage,
    response
  });
};

/**
 * Utility function to generate random number
 * @param {Number} min
 * @param {Number} max
 */
const randomNumberGenerator = (min, max) => Math.floor(Math.random() * (max - min) + min);

module.exports = {
  OK,
  randomNumberGenerator
};
