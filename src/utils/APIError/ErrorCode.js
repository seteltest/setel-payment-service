module.exports = {
  UNSPECIFIED: {
    errTitle: 'Error code not specified',
    errDesc: 'Please try again, if problem still persist, please contact web master',
    errDebugDesc: 'Error code not specified in the system'
  },
  UNKNOWN: {
    errTitle: 'Oops...something went wrong',
    errDesc: 'System is not responding properly',
    errDebugDesc: 'System is not able to handle the error gracefully'
  },
  UNAUTHORIZED: {
    errTitle: 'Access Denied. Invalid Session Token',
    errDesc: 'This name already exist, please choose another name',
    errDebugDesc: 'Client with that name is already exist'
  },
  FORBIDDEN: {
    errTitle: 'Access Denied. Missing Authentication Token.',
    errDesc: 'Missing Authentication Token',
    errDebugDesc: 'Client with that name is already exist'
  },
  NOT_FOUND: {
    errTitle: 'Oops! Something is wrong',
    errDesc: 'The resource you are looking for does not exist!',
    errDebugDesc: 'Client with that name is already exist'
  },
  ERROR_DATABASE_CONNECTIVITY: {
    errTitle: 'Database Connection Error!',
    errDesc: 'Seems we are not able to connect Database',
    errDebugDesc: 'Maybe wrong database information is provided'
  },
  PAYMENT_NOT_CREATED: {
    errTitle: 'Payment failed to initiate',
    errDesc: 'Payment is not able create',
    errDebugDesc: 'Error while creating payment'
  },
  PAYMENT_SERVICE_DOWN: {
    errTitle: 'The payment service down',
    errDesc: 'Seems payment service is not up',
    errDebugDesc: 'Payment service down'
  },
  PAYMENT_NOT_UPDATED: {
    errTitle: 'The payment failed to update',
    errDesc: 'The resource you are trying to update is failed',
    errDebugDesc: 'Payment updation is failed'
  },
  PAYMENT_NOT_FOUND: {
    errTitle: 'The payment you are looking for is not available',
    errDesc: 'The resource you are looking for does not exist!',
    errDebugDesc: 'Payment is removed or never exist in first place'
  }
};
