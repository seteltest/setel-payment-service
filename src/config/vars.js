const path = require('path');

// import .env variables
require('dotenv-safe').load({
  path: path.join(process.cwd(), '.env'),
  sample: path.join(process.cwd(), '.env.example')
});

module.exports = {
  env: process.env.NODE_ENV,
  port: process.env.PORT,
  serviceName: 'setel-payment-service',
  http: {
    timeout: 5000,
    responseType: 'json',
    responseEncoding: 'utf8',
    retries: 3
  },
  sqsConfig: {
    accountId: process.env.SQS_ACCOUNT,
    queueName: process.env.QUEUE_NAME
  },
  PAYMENT_STATUS: {
    PROCESSING: 'PROCESSING',
    PAID: 'PAID',
    REJECTED: 'REJECTED'
  },
  queueSetting: {
    redis: {
      host: process.env.CACHE_HOST,
      port: process.env.CACHE_PORT
    },
    activateDelayedJobs: true,
    removeOnSuccess: true
  }
};
