/* eslint-disable arrow-body-style */
jest.mock('@services/payment');
const { createPayment } = require('@services/payment');
const { APIError } = require('@utils/APIError');
const queue = require('./payment-initiation.queue');

describe('Test payment initiation', () => {
  beforeEach(() => {});

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should throw API Error while creating payment', (done) => {
    createPayment.mockRejectedValue(APIError.withCode('UNSPECIFIED'));
    return queue.paymentInitiateProcess({}).catch((err) => {
      expect(err.errors[0].errorCode).toBe('UNSPECIFIED');
      done();
    });
  });

  it('should return payment object', (done) => {
    createPayment.mockResolvedValue({ id: '1' });
    return queue.paymentInitiateProcess({}).then((response) => {
      expect(response).toBe(true);
      done();
    });
  });
});
