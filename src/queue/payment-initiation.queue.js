/**
 * Order Delivery Job
 *
 */
const { createPayment } = require('@services/payment');
const { logger } = require('@utils/logger');

const paymentInitiateProcess = async (job) => {
  logger.info(`Job ${job.id} start processing`);
  await createPayment(job.data);
  return true;
};

module.exports = {
  paymentInitiateProcess
};
