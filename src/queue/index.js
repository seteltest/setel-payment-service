const Queue = require('bee-queue');
const { queueSetting } = require('@config/vars');
const { paymentInitiateProcess } = require('./payment-initiation.queue');

const paymentCreationQueue = new Queue('paymentCreation', queueSetting);

paymentCreationQueue.process(async job => paymentInitiateProcess(job));

module.exports = {
  paymentCreationQueue
};
