const { updatePaymentStatus } = require('@services/payment');
const { OK, randomNumberGenerator } = require('@utils/helper');
const uuidv4 = require('uuid/v4');

/**
 * process
 * @public
 */
exports.process = async (req, res, next) => {
  try {
    const randomType = randomNumberGenerator(1, 10);
    // below is the mock logic to process the payment
    /* istanbul ignore next */
    let status = 'PAID';
    if (randomType > 5) {
      status = 'REJECTED';
    }
    const transactionId = uuidv4();
    const payment = await updatePaymentStatus(req.params.orderId, status, transactionId);
    return OK(res, 'Payment Updated', payment);
  } catch (err) {
    return next(err);
  }
};
