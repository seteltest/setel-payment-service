const express = require('express');
const validate = require('express-validation');
const controller = require('./process.controller');
const validator = require('./process.validator');

const router = express.Router({ mergeParams: true });

/**
 * @api {post} api/v1/process process
 * @apiDescription Process Payment
 * @apiVersion 1.0.0
 * @apiName process
 * @apiPermission public
 *
 * @apiParam  {String} [param]  Put some parameter schema here
 *
 * @apiSuccess {Number} responseCode     HTTP Response Code
 * @apiSuccess {String} responseMessage  Response message
 * @apiSuccess {Object} response         Response object
 *
 * @apiError (Bad Request 400)  ValidationError  Some parameters may contain invalid values
 */
router.route('/')
  .put(validate(validator.joiSchema), controller.process);

module.exports = router;
