const express = require('express');

const router = express.Router();
const detailRoute = require('./detail');
const processRoute = require('./process');


router.use('/:orderId', processRoute);
router.use('/:orderId', detailRoute);


module.exports = router;
