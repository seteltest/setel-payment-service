const { getPayment } = require('@services/payment');
const { OK } = require('@utils/helper');

/**
 * detail
 * @public
 */
exports.detail = async (req, res, next) => {
  try {
    const payment = await getPayment(req.params.orderId);
    return OK(res, 'Payment Details', payment);
  } catch (err) {
    return next(err);
  }
};
