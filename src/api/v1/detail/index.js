const express = require('express');
const validate = require('express-validation');
const controller = require('./detail.controller');
const validator = require('./detail.validator');

const router = express.Router({ mergeParams: true });

/**
 * @api {get} api/v1/detail detail
 * @apiDescription Payment Detail
 * @apiVersion 1.0.0
 * @apiName detail
 * @apiPermission public
 *
 * @apiParam  {String} [param]  Put some parameter schema here
 *
 * @apiSuccess {Number} responseCode     HTTP Response Code
 * @apiSuccess {String} responseMessage  Response message
 * @apiSuccess {Object} response         Response object
 *
 * @apiError (Bad Request 400)  ValidationError  Some parameters may contain invalid values
 */
router.route('/')
  .get(validate(validator.joiSchema), controller.detail);

module.exports = router;
