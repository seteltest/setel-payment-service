/* eslint-disable arrow-body-style */
jest.mock('@services/payment');
const { getPayment } = require('@services/payment');
const MockReq = require('mock-express-request');
const MockRes = require('mock-express-response');
const httpStatus = require('http-status');
const { APIError } = require('@utils/APIError');
const controller = require('./detail.controller');

describe('Test detail', () => {
  let req;
  let res;

  beforeEach(() => {
    req = new MockReq({
      params: { mediaId: 123 }
    });
    res = new MockRes();
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  test('should failed to give detail object', () => {
    getPayment.mockRejectedValue(APIError.withCode('UNKNOWN'));
    return controller.detail(req, res, (apiError) => {
      expect(apiError).toHaveProperty('name');
      expect(apiError).toHaveProperty('errors');
      expect(apiError.errors[0].errorCode).toBe('UNKNOWN');
    });
  });

  it('should return payment detail object', async () => {
    getPayment.mockResolvedValue({});
    const status = jest.spyOn(res, 'status');
    const json = jest.spyOn(res, 'json');
    return controller.detail(req, res).then(() => {
      expect(status).toBeCalledWith(httpStatus.OK);
      expect(json).toBeCalledWith(expect.objectContaining({
        responseCode: httpStatus.OK,
        responseMessage: expect.any(String),
        response: expect.any(Object)
      }));
    });
  });
});
